use std::path::PathBuf;
use structopt::StructOpt;
use crate::pre_release::PreReleaseVariant;
use log::LevelFilter;



#[derive(Debug, StructOpt)]
#[structopt(
rename_all = "kebab-case",
name = "oxRed",
author = "Yannick Guern <dev@guern.eu>",
about = "Allow to upgrade your package version following the conventional-commit and semver"
)]
pub struct Opt {
    ///Type of pre-release if needed
    #[structopt(name = "pre-release", long, parse(try_from_str), possible_values = &PreReleaseVariant::variants())]
    pub pre_release_option: Option<PreReleaseVariant>,

    /// Path to the git repo to handle
    #[structopt(name = "repo", long, parse(from_os_str), default_value = "." )]
    pub repo_path_option: PathBuf,
    
    /// Level of debug
    #[structopt( 
        name = "level", 
        long, parse(try_from_str), 
        possible_values = &[
            "OFF", "ERROR", "WARN", "INFO", "DEBUG", "TRACE",
            "off", "error", "warn", "info", "debug", "trace",
        ], default_value = "INFO")]
    pub level_option: LevelFilter
}