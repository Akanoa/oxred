use git2::{Repository};
use std::error::Error;
use crate::tag::{Tag, BuildType};
use std::str::from_utf8;
use crate::commit::get_semantic_type_from_list_of_commits;
use structopt::StructOpt;
use crate::command_line::Opt;
use log::LevelFilter;

type ResultCustom<T> = std::result::Result<T, Box<dyn Error>>;

pub mod tag;
mod commit;
mod parsers;
pub mod pre_release;
pub mod command_line;

pub fn exec(opt: Opt) -> ResultCustom<()> {

    let repo_path = opt.repo_path_option.as_path();

    let repo = Repository::open(repo_path)?;

    let head = repo.head()?;
    let last_commit_id = &head.peel_to_commit()?.id().to_string()[..8];
    let branch = head.shorthand().unwrap();

    // Get all tags semantic tags present on repository
    let mut tags = vec![];
    repo.tag_foreach(|oid, tag_name| {

        let tag = match repo.find_tag(oid) {
            Ok(tag) => tag.name().map(|tag_name| (tag_name.to_string(), tag.target_id())),
            Err(_e) => {
                //Maybe a lightweight
                match repo.find_commit(oid) {
                    Ok(commit) => {

                        // the tag_foreach function gives foreach tags a tag name 
                        // with the following fixed pattern refs/tags/xxx
                        // we can can strip of this prefix to get the human readable
                        // tag name
                        let tag_name = &from_utf8(tag_name).unwrap()[10..];

                        let commit_id = commit.id();
                        Some((tag_name.to_string(), commit_id))
                    }
                    Err(_) => None
                }
            }
        };
        
        if let Some((tag_name, target_id)) = tag {
            let tag = Tag::new(tag_name.as_str()).with_target_id(target_id).clone();
            tags.push(tag);
        }

        true
    })?;

    let history = tags;
    
    let relevant_oid = match history.len()  {
        0  => head.target(),
        _ => match history.iter().max() {
            Some(tag) => tag.target_id,
            None => head.target()
        }
    };

    let commits = match relevant_oid {
        None => vec![],
        Some(target_id) => {

            match head.peel_to_commit().ok() {
                None => vec![],
                Some(commit_head) => {
                    let mut commit_messages = vec![];
                    let mut commit = commit_head.clone();

                    loop {

                        match commit.message()  {
                            None => {}
                            Some(message) => commit_messages.push(message.to_string())
                        }

                        if commit.id() == target_id {
                            break;
                        }

                        match commit.parent(0_usize)  {
                            Ok(parent) => {
                                commit = parent;
                            }
                            Err(_) => break
                        }
                    }

                    commit_messages
                }
            }

        }
    };

    // println!("{:?}", commits);
    // println!("{}", TagVec(tags));
    // println!("The current version is: {}", last_tag);
    // println!("On branch {}", &branch);
    // println!("Last commit is {}", last_commit_id);
    // println!("Last tag target_id {}", last_tag.target_id.unwrap());

    let semantic_type = get_semantic_type_from_list_of_commits(
        commits.iter().map(|x|x.as_str()).collect());

    let build_type = match opt.pre_release_option {
        Some(pre_release) => BuildType::PreRelease(pre_release, Some(semantic_type)),
        None => BuildType::Patch
    };
    
    let last_tag = match &head.peel_to_tag()  {
        Ok(_tag) => Tag::new("1.0.0"),
        _ => Tag::new("1.0.0")
    };

    let mut next_tag = last_tag.inc(
        build_type,
        Some(history), branch);
    
    // dbg!("The next tag will be {}", next_tag.with_meta(last_commit_id));

    let tag_name = &next_tag.with_meta(last_commit_id).to_string()[..];
    let target = head.peel_to_commit()?;

    repo.tag_lightweight(tag_name, &target.into_object(), false)?;
    
    Ok(())
}

fn setup_logger(level: LevelFilter) -> Result<(), fern::InitError> {
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}[{}] {}",
                record.target(),
                record.level(),
                message
            ))
        })
        .level(level)
        .chain(std::io::stdout())
        .apply()?;
    Ok(())
}

pub fn run() -> ResultCustom<()>{

    let opt : Opt = Opt::from_args();
    
    exec(opt)?;
    
    Ok(())
}