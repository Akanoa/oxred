use std::cmp::Ordering;
use crate::parsers::parse_tag;
use crate::pre_release::{PreRelease, PreReleaseVariant};
use std::hash::{Hash, Hasher};
use std::fmt::{Display, Formatter};
use std::collections::hash_map::DefaultHasher;
use git2::Oid;

#[repr(u32)]
#[derive(PartialEq, Debug, PartialOrd, Ord, Eq)]
pub enum SemanticType {
    Patch,
    Minor,
    Major,
}

impl SemanticType {
    pub fn inc(&self, tag: &Tag, branch: &str) -> Tag {
         match self  {
            SemanticType::Major if tag.minor != 0 || tag.patch != 0 => tag.inc(BuildType::Major, None, branch),
            SemanticType::Minor if tag.minor == 0 => tag.inc(BuildType::Minor, None, branch),
            SemanticType::Patch if tag.patch == 0 => {
                
                let mut patch = tag.patch + 1;
                if tag.minor != 0 {
                    patch = 0;
                }
                
                Tag {
                    major: tag.major,
                    minor: tag.minor,
                    patch,
                    pre_release: tag.pre_release,
                    meta: tag.meta.as_ref().cloned(),
                    target_id: tag.target_id
                }
            },
            _ => tag.clone()
        }
    }
}

#[derive(Debug)]
pub enum BuildType {
    Major,
    Minor,
    Patch,
    PreRelease(PreReleaseVariant, Option<SemanticType>)
}

impl Display for BuildType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let string = match self {
            BuildType::Major => "major".to_string(),
            BuildType::Minor => "minor".to_string(),
            BuildType::Patch => "patch".to_string(),
            BuildType::PreRelease(variant, semantic) => {
                let variant_str = match variant {
                    PreReleaseVariant::Alpha => "alpha".to_string(),
                    PreReleaseVariant::Beta => "beta".to_string(),
                    PreReleaseVariant::ReleaseCandidate => "rc".to_string(),
                };
                let semantic_str = match semantic {
                    None => "",
                    Some(semantic) => match semantic {
                        SemanticType::Patch => "patch",
                        SemanticType::Minor => "minor",
                        SemanticType::Major => "major",
                    }
                };
                format!("{}-{}", variant_str,  semantic_str )
            }
        };
        f.write_str(string.as_str())
    }
}

#[derive(Debug, Eq)]
pub struct Tag {
    major: u32,
    minor: u32,
    patch: u32,
    pub pre_release: Option<PreRelease>,
    meta: Option<String>,
    pub target_id: Option<Oid>
}

impl Tag {

    pub fn zero() -> Tag {
        Tag {
            major: 0,
            minor: 0,
            patch: 0,
            pre_release: None,
            meta: None,
            target_id: None
        }
    }

    pub fn new(tag_string: &str) -> Tag {
        match parse_tag(tag_string) {
            Ok((_, (pre_release, version, meta))) => Tag {
                major: version.0,
                minor: version.1,
                patch: version.2,
                pre_release,
                meta,
                target_id: None
            },
            Err(_) => Tag::zero()
        }
    }
    
    pub fn with_meta(&mut self, meta: &str) -> &Self {
        self.meta = Some(meta.to_string());
        self
    }
    
    pub fn with_target_id(&mut self, target_id: Oid) -> &Self {
        self.target_id = Some(target_id);
        self
    }
    
    fn calculate_hash(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.hash(&mut hasher);
        hasher.finish()
    }
    
    pub fn create_with_pre_release(&self, pre_release: Option<PreRelease>) -> Tag {
        Tag {
            major: self.major,
            minor: self.minor,
            patch: self.patch,
            pre_release,
            meta: None,
            target_id: self.target_id
        }
    }

    fn inc_with_pre_release(&self, branch: &str, x : u32) -> u32 {

        match branch {
            "master" | "main" => match self.pre_release {
                Some(_) => x,
                None => x+1
            },
            _ => x+1
        }
    }
    
    pub fn inc(&self, build_type: BuildType, history: Option<Vec<Tag>>, branch: &str) -> Tag {
        
        if let Some(history) = history.clone() {
            if history.is_empty() {
                
                match &build_type {
                    BuildType::Major |BuildType::Minor | BuildType::Patch => return self.clone(),
                    _ => {}
                }
            }
        }
        
        match build_type {
            BuildType::Major => Tag {
                major: self.inc_with_pre_release(branch, self.major),
                minor: 0,
                patch: 0,
                pre_release: None,
                meta: None,
                target_id: self.target_id
            },
            BuildType::Minor => Tag {
                major: self.major,
                minor: self.inc_with_pre_release(branch, self.minor),
                patch: 0,
                pre_release: None,
                meta: None,
                target_id: self.target_id
            },
            BuildType::Patch => Tag {
                major: self.major,
                minor: self.minor,
                patch: self.inc_with_pre_release(branch, self.patch),
                pre_release: None,
                meta: None,
                target_id: self.target_id
            },
            BuildType::PreRelease(variant, semantic_type) => {
                match self.pre_release {
                    None => {
                        
                        let mut tag = match semantic_type {
                            None => self.clone(),
                            Some(semantic_type) => {
                                let history = history.clone();
                                match semantic_type {
                                    SemanticType::Major => self.inc(BuildType::Major, history, branch),
                                    SemanticType::Minor => self.inc(BuildType::Minor, history, branch),
                                    SemanticType::Patch => self.inc(BuildType::Patch, history, branch),
                            }
                            }
                        };
                        
                        if let Some(history) = history {
                            if history.is_empty() {
                                tag = self.clone()
                            }
                        }

                        let variant_str = &variant.to_string()[..];
                        tag.create_with_pre_release(PreRelease::new(variant_str, "0"))
                    },
                    Some(pre_release) => {
                        pre_release.inc(variant, semantic_type, history, &self, branch)
                    }
                }
            }
        }
    }
    
    pub fn get_max_from_history(&self, history: Vec<Tag>, build_type: BuildType) -> Option<Tag> {
        
        let tag = match build_type {
            BuildType::PreRelease(variant, ..) => self.create_with_pre_release(PreRelease::new(&variant.to_string()[..], "0")),
            _ => self.clone()
        };
        
        let self_hash = tag.calculate_hash();
        
        history
            .into_iter()
            .filter(|tag | self_hash == tag.calculate_hash())
            .max()
    }
}

impl Clone for Tag {
    fn clone(&self) -> Self {
        Tag {
            major: self.major,
            minor: self.minor,
            patch: self.patch,
            pre_release: self.pre_release,
            meta: self.meta.as_ref().cloned(),
            target_id: self.target_id
        }
    }
}

impl PartialEq for Tag {
    fn eq(&self, other: &Self) -> bool {
        if self.major != other.major {
            return false
        }

        if self.minor != other.minor {
            return false
        }

        if self.patch != other.patch {
            return false
        }

        if self.pre_release != other.pre_release {
            return false
        }

        true
    }
}

impl Ord for Tag {
    fn cmp(&self, other: &Self) -> Ordering {
        if self == other {
            return Ordering::Equal
        }

        if self.major != other.major {
            return self.major.cmp(&other.major)
        }

        if self.minor != other.minor {
            return self.minor.cmp(&other.minor)
        }

        if self.patch != other.patch {
            return self.patch.cmp(&other.patch)
        }

        if let Some(pre_release) = &self.pre_release {
            if let Some(other_pre_release) = &other.pre_release {
                if pre_release < other_pre_release {
                    return Ordering::Less
                }
            }
        }

        Ordering::Greater
    }
}

impl PartialOrd for Tag {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Hash for Tag {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.major.hash(state);
        self.minor.hash(state);
        self.patch.hash(state);
        if let Some(pre_release) = self.pre_release {
            pre_release.hash(state)
        }
    }
}

impl Display for Tag {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {

        let pre_release_str = match self.pre_release {
            None => "".to_string(),
            Some(pre_release) => pre_release.to_string()
        };

        let meta_str = match &self.meta {
            None => "".to_string(),
            Some(meta) => "+".to_string()+ &meta.clone()
        };

        write!(f, "{}.{}.{}{}{}", self.major, self.minor, self.patch, pre_release_str, meta_str)
    }
}

pub struct TagVec(pub Vec<Tag>);

impl Display for TagVec {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {

        let a = self.0.iter().map(|tag| tag.to_string())
            .collect::<Vec<String>>()
            .join(", \n");


        write!(f, "[\n{}\n]", a)
    }
}

#[cfg(test)]
mod tests {
    use crate::pre_release::{PreReleaseVariant, PreRelease};
    use crate::tag::{Tag, BuildType, SemanticType, TagVec};

    #[test]
    fn test_pre_release_variant_ord() {
        assert!(PreReleaseVariant::Alpha < PreReleaseVariant::Beta);
        assert!(PreReleaseVariant::Alpha < PreReleaseVariant::ReleaseCandidate);
        assert_eq!(PreReleaseVariant::Beta, PreReleaseVariant::Beta);
    }

    #[test]
    fn test_create_pre_release() {
        assert_eq!(PreRelease::new("rc", "4"), Some(PreRelease {
            variant: PreReleaseVariant::ReleaseCandidate,
            increment: 4
        }));

        assert_eq!(PreRelease::new("beta", "0"), Some(PreRelease {
            variant: PreReleaseVariant::Beta,
            increment: 0
        }));

        assert_eq!(PreRelease::new("alpha", "666"), Some(PreRelease {
            variant: PreReleaseVariant::Alpha,
            increment: 666
        }));

        assert_eq!(PreRelease::new("toto", "54154154"), None);
    }

    #[test]
    fn test_ordering_by_pre_release() {
        let rc1 = PreRelease::new("rc", "1").unwrap();
        let rc2 = PreRelease::new("rc", "2").unwrap();
        let beta10 = PreRelease::new("beta", "10").unwrap();
        let beta1 = PreRelease::new("beta", "1").unwrap();
        let alpha666 = PreRelease::new("alpha", "666").unwrap();
        let alpha666_bis = PreRelease::new("alpha", "666").unwrap();

        assert!(rc1 < rc2);
        assert!(rc1 > beta10);
        assert!(beta1 < beta10);
        assert!(beta1 < rc2);
        assert!(alpha666 < rc2);
        assert!(alpha666 < rc1);
        assert!(alpha666 < beta10);
        assert!(beta1 > alpha666);
        assert_eq!(alpha666, alpha666_bis)
    }

    #[test]
    fn test_create_tag_from_string() {
        assert_eq!(Tag::new("1.0.0"), Tag {
            major: 1,
            minor: 0,
            patch: 0,
            pre_release: None,
            meta: None,
            target_id: None
        }, "should create tag from semver");

        assert_eq!(Tag::new("99.16.58"), Tag {
            major: 99,
            minor: 16,
            patch: 58,
            pre_release: None,
            meta: None,
            target_id: None
        }, "should create tag from semver");

        assert_eq!(Tag::new("99.16.58-alpha.12"), Tag {
            major: 99,
            minor: 16,
            patch: 58,
            pre_release: PreRelease::new("alpha", "12"),
            meta: None,
            target_id: None
        }, "should create tag from semver with pre release");

        assert_eq!(Tag::new("99.16.58-alpha.12+defd225"), Tag {
            major: 99,
            minor: 16,
            patch: 58,
            pre_release: PreRelease::new("alpha", "12"),
            meta: None,
            target_id: None
        }, "should create tag from semver with pre release and metadata");

        assert_eq!(Tag::new("12.64.0-rc.13+defd225"), Tag {
            major: 12,
            minor: 64,
            patch: 0,
            pre_release: PreRelease::new("rc", "13"),
            meta: None,
            target_id: None
        }, "should create tag from semver with pre release and metadata");
    }

    #[test]
    fn test_ordering_by_major() {
        let tag1 = Tag::new("1.0.0");
        let tag2 = Tag::new("2.0.0");
        let tag3 = Tag::new("5.0.0");

        assert!(tag1 < tag2);
        assert!(tag2 < tag3);
        assert!(tag3 > tag1);
        assert!(tag2 < tag3);
        assert_eq!(tag1, tag1);
    }

    #[test]
    fn test_ordering_by_minor() {
        let tag1 = Tag::new("1.0.0");
        let tag2 = Tag::new("1.1.0");
        let tag3 = Tag::new("1.10.0");

        assert!(tag1 < tag2);
        assert!(tag2 < tag3);
        assert!(tag3 > tag1);
        assert!(tag2 < tag3);
        assert_eq!(tag3, tag3);
    }

    #[test]
    fn test_ordering_by_patch() {
        let tag1 = Tag::new("1.8.0");
        let tag2 = Tag::new("1.8.9");
        let tag3 = Tag::new("1.8.42");

        assert!(tag1 < tag2);
        assert!(tag2 < tag3);
        assert!(tag3 > tag1);
        assert!(tag2 < tag3);
        assert_eq!(tag3, tag3);
    }

    #[test]
    fn test_ordering_version_following_complete_semantic() {
        let tag1 = Tag::new("1.0.0");
        let tag2 = Tag::new("1.0.1");
        let tag3 = Tag::new("1.1.0");
        let tag4 = Tag::new("1.1.1");
        let tag5 = Tag::new("2.0.0");

        let tag12_64_0_rc = Tag::new("12.64.0-rc.13+defd225");
        let tag12_64_0_beta = Tag::new("12.64.0-beta.13+de12a5");
        let tag99_16_58_alpha = Tag::new("99.16.58-alpha.12+defd225");

        assert!(tag1 < tag2);
        assert!(tag2 < tag3);
        assert!(tag3 < tag4);
        assert!(tag1 < tag5);
        assert!(tag2 < tag5);
        assert!(tag3 < tag5);
        assert!(tag4 < tag5);
        assert!(tag4 >= tag3);

        assert!(tag12_64_0_rc > tag12_64_0_beta);
        assert!(tag12_64_0_rc < tag99_16_58_alpha);

        assert_eq!(tag4, tag4);
    }

    #[test]
    fn test_ordering_between_semantic_tags() {

        let tag_1_0_0 =Tag::new("1.0.0");
        let tag_1_0_1_alpha_0 =Tag::new("1.0.1-alpha.0");
        let tag_1_0_1_alpha_1 = Tag::new("1.0.1-alpha.1");
        let tag_1_1_0_alpha_0 =Tag::new("1.1.0-alpha.0");
        let tag_1_1_0_beta_0 =Tag::new("1.1.0-beta.0");

        assert!(tag_1_0_0 < tag_1_0_1_alpha_0);
        assert!(tag_1_0_1_alpha_0 < tag_1_0_1_alpha_1);
        assert!(tag_1_0_1_alpha_1 < tag_1_1_0_beta_0);
        assert!(tag_1_1_0_alpha_0 < tag_1_1_0_beta_0);
        assert!(tag_1_0_1_alpha_0 < tag_1_1_0_beta_0);
        assert!(tag_1_1_0_alpha_0 > tag_1_0_1_alpha_0);
    }

    #[test]
    fn test_inc_major() {
        let tag1 = Tag::new("1.0.0");
        let tag2 = Tag::new("1.8.0");
        let tag3 = Tag::new("1.6.68");
        let tag4 = Tag::new("2.99.1");


        assert_eq!(tag1.inc(BuildType::Major, None, "main"), Tag::new("2.0.0"));
        assert_eq!(tag2.inc(BuildType::Major, None, "main"), Tag::new("2.0.0"));
        assert_eq!(tag3.inc(BuildType::Major, None, "main"), Tag::new("2.0.0"));
        assert_eq!(tag4.inc(BuildType::Major, None, "main"), Tag::new("3.0.0"));
    }

    #[test]
    fn test_inc_minor() {
        let tag1 = Tag::new("1.0.0");
        let tag2 = Tag::new("1.8.0");
        let tag3 = Tag::new("1.6.68");
        let tag4 = Tag::new("2.99.1");

        assert_eq!(tag1.inc(BuildType::Minor, None, "main"), Tag::new("1.1.0"));
        assert_eq!(tag2.inc(BuildType::Minor, None, "main"), Tag::new("1.9.0"));
        assert_eq!(tag3.inc(BuildType::Minor, None, "main"), Tag::new("1.7.0"));
        assert_eq!(tag4.inc(BuildType::Minor, None, "main"), Tag::new("2.100.0"));
    }

    #[test]
    fn test_inc_patch() {
        let tag1 = Tag::new("1.0.0");
        let tag2 = Tag::new("1.8.0");
        let tag3 = Tag::new("1.6.68");
        let tag4 = Tag::new("2.99.1");

        assert_eq!(tag1.inc(BuildType::Patch, None, "main"), Tag::new("1.0.1"));
        assert_eq!(tag2.inc(BuildType::Patch, None, "main"), Tag::new("1.8.1"));
        assert_eq!(tag3.inc(BuildType::Patch, None, "main"), Tag::new("1.6.69"));
        assert_eq!(tag4.inc(BuildType::Patch, None, "main"), Tag::new("2.99.2"));
    }

    #[test]
    fn test_inc_rc() {

        let tag1 = Tag::new("1.0.0");
        let tag2 = Tag::new("1.8.0");
        let tag3 = Tag::new("1.6.68");
        let tag4 = Tag::new("2.99.1-beta.4");
        let tag5 = Tag::new("8.7.0-alpha.3");
        let tag6 = Tag::new("8.7.0-alpha.3+e782585");
        let tag7 = Tag::new("1.1.1-rc.0");

        assert_eq!(tag1.inc(BuildType::PreRelease(PreReleaseVariant::ReleaseCandidate, Some(SemanticType::Patch)), None, "develop"),
                   Tag::new("1.0.1-rc.0"));
        assert_eq!(tag2.inc(BuildType::PreRelease(PreReleaseVariant::ReleaseCandidate, Some(SemanticType::Minor)), None, "develop"),
                   Tag::new("1.9.0-rc.0"));
        assert_eq!(tag3.inc(BuildType::PreRelease(PreReleaseVariant::ReleaseCandidate, Some(SemanticType::Major)), None, "develop"),
                   Tag::new("2.0.0-rc.0"));
        assert_eq!(tag4.inc(BuildType::PreRelease(PreReleaseVariant::ReleaseCandidate, None), None, "develop"),
                   Tag::new("2.99.1-rc.0"));
        assert_eq!(tag5.inc(BuildType::PreRelease(PreReleaseVariant::ReleaseCandidate, None), None, "develop"),
                   Tag::new("8.7.0-rc.0"));
        assert_eq!(tag6.inc(BuildType::PreRelease(PreReleaseVariant::ReleaseCandidate, None), None, "develop"),
                   Tag::new("8.7.0-rc.0"));
        assert_eq!(tag7.inc(BuildType::PreRelease(PreReleaseVariant::ReleaseCandidate, None), None, "develop"),
                   Tag::new("1.1.1-rc.1"));
    }

    #[test]
    fn test_inc_beta() {

        let tag1 = Tag::new("1.0.0");
        let tag2 = Tag::new("1.8.0");
        let tag3 = Tag::new("1.6.68");
        let tag4 = Tag::new("2.99.1-beta.4");
        let tag5 = Tag::new("8.7.0-alpha.3");
        let tag6 = Tag::new("8.7.0-alpha.3+e782585");
        let tag7 = Tag::new("1.1.1-rc.0");

        assert_eq!(tag1.inc(BuildType::PreRelease(PreReleaseVariant::Beta, Some(SemanticType::Patch)), None, "release/21.07"),
                   Tag::new("1.0.1-beta.0"));
        assert_eq!(tag2.inc(BuildType::PreRelease(PreReleaseVariant::Beta, Some(SemanticType::Minor)), None, "release/21.07"),
                   Tag::new("1.9.0-beta.0"));
        assert_eq!(tag3.inc(BuildType::PreRelease(PreReleaseVariant::Beta, Some(SemanticType::Major)), None, "release/21.07"),
                   Tag::new("2.0.0-beta.0"));
        assert_eq!(tag4.inc(BuildType::PreRelease(PreReleaseVariant::Beta, None), None, "release/21.07"),
                   Tag::new("2.99.1-beta.5"));
        assert_eq!(tag5.inc(BuildType::PreRelease(PreReleaseVariant::Beta, None), None, "release/21.07"),
                   Tag::new("8.7.0-beta.0"));
        assert_eq!(tag6.inc(BuildType::PreRelease(PreReleaseVariant::Beta, None), None, "release/21.07"),
                   Tag::new("8.7.0-beta.0"));
        assert_eq!(tag7.inc(BuildType::PreRelease(PreReleaseVariant::Beta, None), None, "release/21.07"),
                   Tag::new("1.1.1-beta.0"));
    }

    #[test]
    fn test_display_vec_of_tags() {
        let mut history = vec![
            Tag::new("1.0.1-alpha.1"),
            Tag::new("1.0.1-alpha.0"),
            Tag::new("1.1.0-beta.0"),
            Tag::new("1.0.2-beta.0+e5f6de"),
            Tag::new("1.0.2-beta.0-tarte1255_fdsgfdh"),
            Tag::new("1.0.0"),
            Tag::new("1.1.0-alpha.0"),
        ];

        history.sort();

        let result = format!("{}", TagVec(history));
        let expected = "[
1.0.0, 
1.0.1-alpha.0, 
1.0.1-alpha.1, 
1.0.2-beta.0+e5f6de, 
1.0.2-beta.0, 
1.1.0-alpha.0, 
1.1.0-beta.0
]";
        assert_eq!(result, expected)
    }
    
    #[test]
    fn test_get_max_tag_filter_version() {
        let history = vec![
            Tag::new("1.0.1-alpha.1"),
            Tag::new("1.0.1-alpha.2"),
            Tag::new("1.0.1-alpha.0"),
            Tag::new("1.0.1-beta.6"),
            Tag::new("1.0.1-rc.4"),
            Tag::new("1.1.0-beta.0"),
            Tag::new("1.0.2-beta.0+e5f6de"),
            Tag::new("1.0.0"),
            Tag::new("1.1.0-alpha.0"),
        ];
        
        let tag = Tag::new("1.0.1-beta.45");
        
        let expected = Tag::new("1.0.1-alpha.2");
        let result = tag.get_max_from_history(history, BuildType::PreRelease(PreReleaseVariant::Alpha, None));
        
        assert_eq!(result, Some(expected));
    }

    #[test]
    fn test_inc_alpha() {

        let tag1 = Tag::new("1.0.0");
        let tag2 = Tag::new("1.8.0");
        let tag3 = Tag::new("1.6.68");
        let tag4 = Tag::new("2.99.1-beta.4");
        let tag5 = Tag::new("8.7.0-alpha.3");
        let tag6 = Tag::new("8.7.0-alpha.3+e782585");
        let tag7 = Tag::new("1.1.1-rc.0");

        assert_eq!(tag1.inc(BuildType::PreRelease(PreReleaseVariant::Alpha, Some(SemanticType::Patch)), None, "feature/1-my-branch"),
                   Tag::new("1.0.1-alpha.0"));
        assert_eq!(tag2.inc(BuildType::PreRelease(PreReleaseVariant::Alpha, Some(SemanticType::Minor)), None, "feature/1-my-branch"),
                   Tag::new("1.9.0-alpha.0"));
        assert_eq!(tag3.inc(BuildType::PreRelease(PreReleaseVariant::Alpha, Some(SemanticType::Major)), None, "feature/1-my-branch"),
                   Tag::new("2.0.0-alpha.0"));
        assert_eq!(tag4.inc(BuildType::PreRelease(PreReleaseVariant::Alpha, None), None, "feature/1-my-branch"),
                   Tag::new("2.99.1-alpha.0"));
        assert_eq!(tag5.inc(BuildType::PreRelease(PreReleaseVariant::Alpha, None), None, "feature/1-my-branch"),
                   Tag::new("8.7.0-alpha.4"));
        assert_eq!(tag6.inc(BuildType::PreRelease(PreReleaseVariant::Alpha, None), None, "feature/1-my-branch"),
                   Tag::new("8.7.0-alpha.4"));
        assert_eq!(tag7.inc(BuildType::PreRelease(PreReleaseVariant::Alpha, None), None, "feature/1-my-branch"),
                   Tag::new("1.1.1-alpha.0"));
    }
    
    #[test]
    fn test_semantic_type_inc() {
        // Note : This case can't exist in a real world usage
        // let result = SemanticType::inc(&SemanticType::Major, &Tag::new("1.0.0"), "main");
        // assert_eq!(result, Tag::new("2.0.0"));

        let result = SemanticType::inc(&SemanticType::Minor, &Tag::new("1.0.0"), "main");
        assert_eq!(result, Tag::new("1.1.0"));

        let result = SemanticType::inc(&SemanticType::Patch, &Tag::new("1.0.0"), "main");
        assert_eq!(result, Tag::new("1.0.1"));

        let result = SemanticType::inc(&SemanticType::Minor, &Tag::new("1.0.1-beta.1"), "feature/1-my-branch");
        let result = result.create_with_pre_release(PreRelease::new("alpha", "1"));
        assert_eq!(result, Tag::new("1.1.0-alpha.1"));
    }
    
    #[test]
    fn test_inc_version_full_scenario() {
        
        let mut history = vec![
          Tag::new("1.0.0")  
        ];
        
        // 1 - On a feature branch a new alpha of fix version is created
        let expected = Tag::new("1.0.1-alpha.0");
        let last_tag = Tag::new("1.0.0");
        
        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::Alpha,
                                                                Some(SemanticType::Patch)), Some(history.clone()),
                                  "feature/1-my-branch");
        
        history.push(result.clone());
        assert_eq!(result, expected);
        
        // 2 - New alpha of fix version on the same feature branch
        let expected = Tag::new("1.0.1-alpha.1");
        let last_tag = result;

        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::Alpha,
                                                                Some(SemanticType::Patch)), Some(history.clone()),
                                  "feature/1-my-branch");

        history.push(result.clone());
        assert_eq!(result, expected);
        
        // 3 - Merge of the feature branch on release branch to create a beta version
        let expected = Tag::new("1.0.1-beta.0");
        let last_tag = result;

        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::Beta,
                                                                Some(SemanticType::Patch)), Some(history.clone()),
                                  "release/21.07");

        history.push(result.clone());
        assert_eq!(result, expected);
        
        // 4 - In parallel we create another alpha fix version
        
        // The history is cleanup
        let mut history2 = vec![
            Tag::new("1.0.0")
        ];
        
        let expected = Tag::new("1.0.1-alpha.0");
        let last_tag = Tag::new("1.0.0");

        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::Alpha,
                                                                Some(SemanticType::Patch)), Some(history2.clone()),
                                  "feature/2-my-branch-2");

        history2.push(result.clone());
        assert_eq!(result, expected);
        
        // 5 - And one more
        let expected = Tag::new("1.0.1-alpha.1");
        let last_tag = result;

        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::Alpha,
                                                                Some(SemanticType::Patch)), Some(history2.clone()),
                                  "feature/2-my-branch-2");

        history.push(result.clone());
        assert_eq!(result, expected);

        // 6 - Merge of this other feature branch on release branch to create a beta version
        let expected = Tag::new("1.0.1-beta.1");
        let last_tag = result;

        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::Beta,
                                                                Some(SemanticType::Patch)), Some(history.clone()),
                                  "release/21.07");

        history.push(result.clone());
        assert_eq!(result, expected);
        
        // 6 - Create a new alpha fix version on feature branch derived from merge commit
        let expected = Tag::new("1.0.1-alpha.2");
        let last_tag = result;

        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::Alpha,
                                                                Some(SemanticType::Patch)), Some(history.clone()),
                                  "feature/3-my-branch-3");

        assert_eq!(result, expected);
        
        // 7 - Merge this feature branch to release branch and create a fix beta version
        let expected = Tag::new("1.0.1-beta.2");
        let last_tag = result;

        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::Beta,
                                                                Some(SemanticType::Patch)), Some(history.clone()),
                                  "release/21.07");

        history.push(result.clone());
        assert_eq!(result, expected);
        
        // 8 - Create a new alpha feature version on feature branch derived from 1.0.1-beta.1 merge commit
        let expected = Tag::new("1.1.0-alpha.0");
        let last_tag = Tag::new("1.0.1-beta.1");
        
        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::Alpha,
                                                                Some(SemanticType::Minor)), Some(history.clone()),
                                  "feature/3-my-branch-3");

        history.push(result.clone());
        assert_eq!(result, expected);
        
        // 9 - Merge the previous feature to the release branch and a feature beta version
        let expected =  Tag::new("1.1.0-beta.0");
        let last_tag = result;

        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::Beta,
                                                                Some(SemanticType::Minor)), Some(history.clone()),
                                  "release/21.07");

        history.push(result.clone());
        assert_eq!(result, expected);
        
        // 10 - Merge the release branch to develop branch and create a feature release version
        let expected =  Tag::new("1.1.0-rc.0");
        let last_tag = result;

        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::ReleaseCandidate,
                                                                Some(SemanticType::Minor)), Some(history.clone()),
                                  "develop");

        history.push(result.clone());
        assert_eq!(result, expected);
        
        // 11 - Create an hotfix branch derived from release candidate with an alpha breaking change fix version
        let expected =  Tag::new("2.0.0-alpha.0");
        let last_tag = Tag::new("1.1.0-beta.0");

        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::Alpha,
                                                                Some(SemanticType::Major)), Some(history.clone()),
                                  "hotfix/4-on-21.07");

        history.push(result.clone());
        assert_eq!(result, expected);
        
        // 12 - Merge the hotfix branch and create a beta breaking change fix version
        let expected =  Tag::new("2.0.0-beta.0");
        let last_tag = result;

        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::Beta,
                                                                Some(SemanticType::Major)), Some(history.clone()),
                                  "release/21.07");

        history.push(result.clone());
        assert_eq!(result, expected);
        
        // 13 - Merge release branch to develop and create a rc breaking change fix version
        let expected =  Tag::new("2.0.0-rc.0");
        let last_tag = result;

        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::ReleaseCandidate,
                                                                Some(SemanticType::Major)), Some(history.clone()),
                                  "develop");

        history.push(result.clone());
        assert_eq!(result, expected);
        
        // 14 - Merge develop branch to master and create a breaking change 
        let expected =  Tag::new("2.0.0");
        let last_tag = result;

        let result = last_tag.inc(BuildType::Major, Some(history.clone()), "main");

        history.push(result.clone());
        assert_eq!(result, expected);
    }
    
    #[test]
    fn test_inc_edge_case() {
        let history = vec![
            Tag::new("1.0.0"),
            Tag::new("1.0.1-alpha.1+56528948")
        ];

        let expected =  Tag::new("1.1.0-alpha.0");
        let last_tag = Tag::new("1.0.1-alpha.1+56528948");

        let result = last_tag.inc(BuildType::PreRelease(PreReleaseVariant::Alpha, Some(SemanticType::Minor)), 
                                  Some(history.clone()), "feature/1-my-super-fix");

        assert_eq!(result, expected);
    }
    
    #[test]
    fn test_inc_edge_case_minor_after() {
        
        let history = vec![
            Tag::new("1.0.0"),
            Tag::new("1.0.1-alpha.1+56528948"),
            Tag::new("1.1.0-alpha.0+877c3204"),
            Tag::new("1.1.0-alpha.1+b4c118cd"),
        ];
        
        let commits = vec!["fix: pour phisyx\n"];

        let last_tag = &history.iter().max().unwrap();

        let new_tag = last_tag.inc(
            BuildType::PreRelease(PreReleaseVariant::Alpha, Some(SemanticType::Patch)),
            Some(history.clone()), "feature/1-my-super-fix");
        
        assert_eq!(new_tag, Tag::new("1.1.0-alpha.2") )

    }
    
    #[test]
    fn test_edge_case_patch_no_tag_history() {
        let history = vec![];
        
        let last_tag = Tag::new("1.0.0");
        
        let new_tag = last_tag.inc(
          BuildType::PreRelease(PreReleaseVariant::Alpha, Some(SemanticType::Patch)),
            Some(history),
            "feature/1"
        );
        
        assert_eq!(new_tag, Tag::new("1.0.0-alpha.0"))
    }

    #[test]
    fn test_edge_case_release_no_tag_history() {
        let history = vec![];

        let last_tag = Tag::new("1.0.0");

        let new_tag = last_tag.inc(
            BuildType::Patch,
            Some(history),
            "feature/1"
        );

        assert_eq!(new_tag, Tag::new("1.0.0"))
    }
}
