use std::error::Error;

use nom::IResult;
use nom::combinator::{map_opt, map_res, opt};
use nom::character::complete::{digit1, alpha1, anychar, space1, newline};
use nom::bytes::complete::{tag, take_until};
use nom::sequence::tuple;
use nom::branch::alt;
use nom::multi::many0;

use crate::pre_release::PreRelease;
use crate::tag::SemanticType;


type Result<T> = std::result::Result<T, Box<dyn Error>>;
type FullTagVersion = (Option<PreRelease>, (u32, u32, u32), Option<String>);

fn parse_digit(input: &str) -> IResult<&str, u32> {
    
    map_opt(
        digit1,
        |x: &str| x.parse::<u32>().ok()
        
    )(input)
}

fn parse_version(input: &str) -> IResult<&str, (u32, u32, u32)> {

    let (input, (major, _, minor, _, patch)) =
        tuple((parse_digit, tag("."), parse_digit, tag("."), parse_digit))(input)?;

    Ok((input, (major, minor, patch)))

}

fn parse_pre_release(input: &str) -> IResult<&str, Option<PreRelease>> {
    map_res(
        tuple((tag("-"), alpha1, tag("."), digit1)),
        |(_, variant,_, increment )| -> Result<Option<PreRelease>> {
            Ok(PreRelease::new(variant, increment))
        }
    )(input)
}

fn parse_meta_details(input: &str) -> IResult<&str, String> {
    map_res(tuple((tag("+"), many0(anychar))),
        |(_, meta)| -> Result<String> {
            Ok(meta.into_iter().collect::<String>())
        }
    )(input)
}

pub fn parse_tag(input: &str) -> IResult<&str, FullTagVersion> {
    let (input, version) = parse_version(input)?;
    
    let (input, pre_release) = opt(parse_pre_release)(input)?;
    let (input, pre_release) = match pre_release {
        None => (input, None),
        Some(pre_release) => (input, pre_release),
    };

    let (input, meta) = opt(parse_meta_details)(input)?;
    let (input, meta) = match meta {
        None => (input, None),
        Some(meta) => (input, Some(meta)),
    };

    Ok((input, (
        pre_release,
        version,
        meta
    )))
}

pub fn parse_commit(input: &str) -> IResult<&str, SemanticType> {
    
    let parser_commit_type = map_res(
        alt((tag("fix"), tag("feat"))), 
        |commit_type_str: &str| -> Result<SemanticType> {
            match commit_type_str {
                "feat" => Ok(SemanticType::Minor),
                _  => Ok(SemanticType::Patch)
            }
        });
    
    let mut header = tuple((
        parser_commit_type, 
        opt(tag("!")), 
        tag(":"), 
        space1, 
        opt(take_until("\n")),
        opt(newline)
    ));
    
    let mut body = tuple((
        opt(tag("BREAKING CHANGE:")), 
        many0(anychar)
    ));
    
    let (input, (commit_type, short_breaking, ..)) = header(input)?;
    let (input, _) = opt(newline)(input)?;
    let (input, (literal_breaking, ..)) = body(input)?;
    
    let commit_type = match short_breaking {
        Some(_) => SemanticType::Major,
        _ => commit_type
    };

    let commit_type = match literal_breaking {
        Some(_) => SemanticType::Major,
        _ => commit_type
    };
    
    Ok((input, commit_type))
}

#[cfg(test)]
mod tests {
    use crate::parsers::{parse_pre_release, parse_commit};
    use crate::pre_release::PreRelease;
    use crate::tag::SemanticType;

    #[test]
    fn test_parse_pre_release() {
        assert_eq!(parse_pre_release("-alpha.12"), Ok(
            ("", PreRelease::new("alpha", "12"))
        ));

        assert_eq!(parse_pre_release("-toto.12"), Ok(
            ("", None)
        ))
    }

    #[test]
    fn test_parse_commit() {
        
        let feature_commit = "feat: Add a new commit";
        
        assert_eq!(parse_commit(feature_commit), Ok(
            ("", SemanticType::Minor)
        ));

        let fix_commit = "fix: Correct something";

        assert_eq!(parse_commit(fix_commit), Ok(
            ("", SemanticType::Patch)
        ));

        let breaking_change_fix_commit = "fix!: Correct something but break compatibility";

        assert_eq!(parse_commit(breaking_change_fix_commit), Ok(
            ("", SemanticType::Major)
        ));

        let breaking_change_feature_commit = "feat!: Add something but break something else";

        assert_eq!(parse_commit(breaking_change_feature_commit), Ok(
            ("", SemanticType::Major)
        ));
        
        let breaking_change_feature_with_body = "feat: Add a new way to connect

BREAKING CHANGE: Drop the old connection way";

        assert_eq!(parse_commit(breaking_change_feature_with_body), Ok(
            ("", SemanticType::Major)
        ));

        let breaking_change_fix_with_body = "feat: Try to fix some bugs

BREAKING CHANGE: But break everything";

        assert_eq!(parse_commit(breaking_change_fix_with_body), Ok(
            ("", SemanticType::Major)
        ));
    }
}
