use std::cmp::Ordering;
use std::hash::{Hash, Hasher};
use std::fmt::{Display, Formatter};
use crate::tag::{Tag, BuildType, SemanticType};
use std::str::FromStr;

#[repr(u32)]
#[derive(PartialOrd, PartialEq, Debug, Eq, Copy, Clone, Hash)]
pub enum PreReleaseVariant {
    Alpha,
    Beta,
    ReleaseCandidate
}

impl PreReleaseVariant {
    
    pub fn from_string(variant_string: &str) -> Option<PreReleaseVariant> {
        match variant_string {
            "rc" => Some(PreReleaseVariant::ReleaseCandidate),
            "beta" => Some(PreReleaseVariant::Beta),
            "alpha" => Some(PreReleaseVariant::Alpha),
            _ => None
        }
    }
    
    pub fn variants<'a>() -> Vec<&'a str> {
        vec![
            "rc",
            "beta",
            "alpha"
        ]
    }
}

impl FromStr for PreReleaseVariant {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        PreReleaseVariant::from_string(s).ok_or_else(||" ".to_string())
    }
}

impl Display for PreReleaseVariant {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {

        let variant_str = match self  {
            PreReleaseVariant::ReleaseCandidate => "rc",
            PreReleaseVariant::Beta => "beta",
            PreReleaseVariant::Alpha => "alpha",
        };
        
        f.write_str(variant_str)
    }
}

#[derive(Debug, Eq, Copy, Clone)]
pub struct PreRelease {
    pub(crate) variant: PreReleaseVariant,
    pub(crate) increment: u32
}

impl PreRelease {
    pub fn new(variant_str: &str, increment: &str) -> Option<PreRelease> {
        let variant = PreReleaseVariant::from_string(variant_str);
        match variant {
            Some(variant) => {
                match increment.parse() {
                    Ok(increment) => Some(PreRelease {variant, increment}),
                    _ => None
                }
            },
            _ => None
        }
    }

    pub fn inc(&self, variant: PreReleaseVariant, semantic_type: Option<SemanticType>, history : Option<Vec<Tag>>, tag_source: &Tag, branch: &str) -> Tag {

        let variant_string = &variant.to_string()[..];
        
        let tag = match semantic_type {
            None => tag_source.clone(),
            Some(semantic_type) => SemanticType::inc(&semantic_type, tag_source, branch)
        };

        let mut increment = match self.variant == variant {
            true => self.increment + 1,
            _ => match history {
                None => 0,
                Some(history) => {
                    let found = tag.get_max_from_history(history, BuildType::PreRelease(variant, None));
                    match found {
                        None => 0,
                        Some(tag) => match tag.pre_release {
                            None => 0,
                            Some(pre_release) => pre_release.increment + 1
                        }
                    }
                } 
                
            }
        };
        
        // When the version of the pre-release tag is altered
        // reset the pre-release counter
        if &tag != tag_source {
            increment = 0
        }

        let pre_release = PreRelease::new(variant_string, &increment.to_string()[..]).unwrap();
        
        tag.create_with_pre_release(Some(pre_release))
    }
}

impl PartialEq for PreRelease {
    fn eq(&self, other: &Self) -> bool {

        if self.variant != other.variant {
            return false
        }

        if self.increment != other.increment {
            return false
        }

        true
    }
}

impl Ord for PreRelease {
    fn cmp(&self, other: &Self) -> Ordering {

        if self == other {
            return Ordering::Equal
        }

        if self.variant < other.variant {
            return Ordering::Less
        }

        if self.variant == other.variant && self.increment < other.increment {
            return Ordering::Less
        }

        Ordering::Greater
    }
}

impl PartialOrd for PreRelease {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Hash for PreRelease {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.variant.hash(state);
    }
}

impl Display for PreRelease {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "-{}.{}", self.variant.to_string(), self.increment)
    }
}
