use crate::tag::SemanticType;
use crate::parsers::parse_commit;

pub fn get_semantic_type_from_list_of_commits(commits: Vec<&str>) -> SemanticType {
    commits.iter()
        .map(|&x| match parse_commit(x) {
            Ok((_, semantic_type)) => semantic_type, 
            Err(_) => SemanticType::Minor
        })
        .max()
        .unwrap()
}

#[cfg(test)]
mod tests {
    use crate::commit::get_semantic_type_from_list_of_commits;
    use crate::tag::SemanticType;

    #[test]
    fn test_get_semantic_type_from_list_of_commits() {
        let feature_commit = "feat: Add a new commit";
        let feature_commit2 = "feat: Add a new commit 2";
        let fix_commit = "fix: Correct something";
        let fix_commit2 = "fix: Correct something 2";
        let breaking_change_fix_commit = "fix!: Correct something but break compatibility";
        
        let vec1 = vec![feature_commit.clone(), fix_commit.clone()];
        assert_eq!(get_semantic_type_from_list_of_commits(vec1), SemanticType::Minor);

        let vec2 = vec![fix_commit.clone(), feature_commit.clone()];
        assert_eq!(get_semantic_type_from_list_of_commits(vec2), SemanticType::Minor);

        let vec3 = vec![fix_commit.clone(), fix_commit2.clone()];
        assert_eq!(get_semantic_type_from_list_of_commits(vec3), SemanticType::Patch);

        let vec4 = vec![feature_commit.clone(), feature_commit2.clone()];
        assert_eq!(get_semantic_type_from_list_of_commits(vec4), SemanticType::Minor);

        let vec5 = vec![feature_commit.clone(), feature_commit2.clone(), breaking_change_fix_commit.clone()];
        assert_eq!(get_semantic_type_from_list_of_commits(vec5), SemanticType::Major);            
        
    }
    
}
