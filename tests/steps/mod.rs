use cucumber::Steps;
use crate::{GitRepository};
use git2::{Repository, Time, Signature};
use std::rc::Rc;
use semantic_version::exec;
use semantic_version::command_line::Opt;
use semantic_version::pre_release::{PreRelease, PreReleaseVariant};
use semantic_version::tag::{Tag, BuildType};
use log::LevelFilter;

fn get_commit_messages(repo_clone: Rc<Repository>) -> Vec<String>{
    
    let mut messages = vec![];
    let mut commit = repo_clone
        .head()
        .expect("Can't find HEAD")
        .peel_to_commit()
        .expect("Can't find HEAD commit");
    
    loop {
        
        if let Some(message) = commit.message() {
            messages.push(message.to_string())
        }
                    
        if let Ok(parent) = commit.parent(0) {
            commit = parent;
        } else {
            break
        }
                
    }
    
    messages
}

fn get_last_tag(repo: &Rc<Repository>) -> Tag {
    
    let tag_names = repo.tag_names(None).unwrap();
    let tag_name = tag_names.get(0).unwrap();
    
    Tag::new(tag_name)
}

pub fn steps() -> Steps<GitRepository> {
    let mut steps = Steps::<GitRepository>::new();
    
    steps.given("un projet versionné sous git ayant seulement un commit initial", |git_repository, _| {

        let epoch = Time::new(0_i64, 0_i32);
        let signature = Signature::new(
            "test", 
            "test@example.com", &epoch)
            .expect("Can't create signature");
        
        let tree_id = {|repo: &Repository| {
            let mut index = repo.index().unwrap();
            index.write_tree().unwrap()
        }}(git_repository.repo.as_ref());

        let repo_clone = git_repository.repo.clone();
        let tree = repo_clone.find_tree(tree_id).unwrap();

        git_repository.repo.commit(
            Some("HEAD"), 
            &signature, 
            &signature, 
            "Initial commit", &tree, &[])
            .expect("Can't create an initial commit");
        
        git_repository
    });
    
    steps.when_regex(r##"je créé un commit avec le message "([\w\s':.\n!]+)""##, |git_repository, ctx| {
        let message = ctx.matches.get(1).expect("Can't find commit message");
        
        let signature = git_repository.repo.signature().expect("Can't find signature");
        
        let repo_clone = git_repository.repo.clone();
        let head = repo_clone.head().expect("Can't find the HEAD");
        
        let parent = head.peel_to_commit().expect("Can't get commit from HEAD");
        
        let tree = head.peel_to_tree().expect("Can't find tree for head commit");

        let repo_clone = git_repository.repo.clone();
        repo_clone.commit(
            Some("HEAD"),
            &signature,
            &signature,
            message, &tree, &[&parent])
            .expect("Can't create commit");

        let repo_clone = git_repository.repo.clone();
        get_commit_messages(repo_clone);
        
        git_repository
    });
    
    steps.when_regex(r"je créé une version (alpha|beta|rc)", |git_repository, ctx| {

        let variant_str = ctx.matches.get(1).expect("Can't find variant");
        
        let variant = PreReleaseVariant::from_string(variant_str);
        
        let path = git_repository.temp_dir.clone().path().to_path_buf();
        
        exec(Opt {
            pre_release_option: variant,
            repo_path_option: path,
            level_option: LevelFilter::Off
        });
        
        git_repository
    });

    steps.when(r"je créé une version de release", |git_repository, _| {
        
        let path = git_repository.temp_dir.clone().path().to_path_buf();
        
        exec(Opt {
            pre_release_option: None,
            repo_path_option: path,
            level_option: LevelFilter::Off
        });
    
        git_repository
    });
    
    steps.then("je dois avoir les bons commits", |git_repository, _ctx|{

        let repo_clone = git_repository.repo.clone();
        let messages = get_commit_messages(repo_clone);
        
        let messages = &messages[.. messages.len() - 1];
        
        let expected = vec![
            "feat: Ma super fonctionnalité".to_string(),
            "fix: Mon super fix".to_string(),
        ];
        
        assert_eq!(messages, &expected);
        
        git_repository
    });
    
    steps.then_regex(r"le nouveau tag est: (\d+.\d+.\d+-(?:alpha|beta|rc)\.\d+)$", |git_repository, ctx| {

        let version = ctx.matches.get(1).expect("Can't find version");
        let expected = Tag::new(version);
        
        let repo = git_repository.repo.clone();
        let result_tag = get_last_tag(&repo);
        
        assert_eq!(expected, result_tag);
        
        git_repository
    });

    steps.then_regex(r"le nouveau tag est: (\d+.\d+.\d+)$", |git_repository, ctx| {

        let version = ctx.matches.get(1).expect("Can't find version");
        let expected = Tag::new(version);

        let repo = git_repository.repo.clone();
        let result_tag = get_last_tag(&repo);

        assert_eq!(expected, result_tag);

        git_repository
    });
    
    steps
}