use cucumber::{World, Cucumber};
use std::convert::Infallible;
use async_trait::async_trait;
use std::error::Error;
use git2::{Repository};
use tempdir::TempDir;
use std::rc::Rc;

mod steps;

type ResultMain<T> = std::result::Result<T, Box<dyn Error>>;

pub struct GitRepository {
    repo: Rc<Repository>,
    #[allow(dead_code)]
    temp_dir: Rc<TempDir>
}

#[async_trait(?Send)]
impl World for GitRepository {
    type Error = Infallible;

    async fn new() -> Result<Self, Self::Error> {

        let tempdir_instance = TempDir::new("").expect("Can't create directory");
        let repo_path = tempdir_instance.path();
        let repo = Repository::init(repo_path).expect("Can't initialize repository");
        
        Ok(GitRepository {
            repo: Rc::new(repo),
            temp_dir: Rc::new(tempdir_instance)
        })
    }
}

#[tokio::main]
async fn main() -> ResultMain<()> {
    
    Cucumber::<GitRepository>::new()
        .features(&["./tests/features"])
        .steps(steps::steps())
        .cli()
        .debug(true)
        .run()
        .await;
    
    Ok(())
}

