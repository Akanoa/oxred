# language: fr
  
Fonctionnalité: Première version sur un projet vide
  
  Scénario: Création d'une version alpha
    Etant donné un projet versionné sous git ayant seulement un commit initial
    Quand je créé un commit avec le message "fix: Mon super fix" 
    Et je créé un commit avec le message "feat: Ma super fonctionnalité"
    Et je créé une version alpha
    Alors le nouveau tag est: 1.0.0-alpha.0
    
  Scénario: Création d'une version bêta
    Etant donné un projet versionné sous git ayant seulement un commit initial
    Quand je créé un commit avec le message "fix: Mon super fix"
    Et je créé un commit avec le message "feat: Ma super fonctionnalité"
    Et je créé une version beta
    Alors le nouveau tag est: 1.0.0-beta.0

  Scénario: Création d'une version release candidate
    Etant donné un projet versionné sous git ayant seulement un commit initial
    Quand je créé un commit avec le message "fix: Mon super fix"
    Et je créé un commit avec le message "feat: Ma super fonctionnalité"
    Et je créé une version rc
    Alors le nouveau tag est: 1.0.0-rc.0

  Scénario: Création d'une version de release
    Etant donné un projet versionné sous git ayant seulement un commit initial
    Quand je créé un commit avec le message "fix: Mon super fix"
    Et je créé un commit avec le message "feat: Ma super fonctionnalité"
    Et je créé une version de release
    Alors le nouveau tag est: 1.0.0